from unittest import TestCase
from unittest.mock import Mock, patch
from flask_cloud_services.services.storage import (
    upload_from_filename,
    download_from_filename,
    get_file_url,
    upload_from_file_obj,
)


def resolve_patch(path: str) -> str:
    return f'flask_cloud_services.services.storage.{path}'


class TestStorage(TestCase):

    @patch(resolve_patch('factory'))
    def test_upload_from_filename(self, mock_factory):
        # Prepare and Mocks
        data = dict(
            filename='/a/-filename',
            bucket_name='a-bucket'
        )
        mock_upload = Mock(return_value=None)
        mock_factory.return_value.upload_from_filename = mock_upload
        result = upload_from_filename(**data)
        self.assertIsNone(result)
        mock_upload.assert_called_once_with(**data)

    @patch(resolve_patch('factory'))
    def test_upload_from_file_obj(self, mock_factory):
        # Prepare and Mocks
        data = dict(
            file_obj=b'file_obj',
            bucket_name='a-bucket',
            key='any-key'
        )
        mock_upload = Mock(return_value=None)
        mock_factory.return_value.upload_from_file_obj = mock_upload
        result = upload_from_file_obj(**data)
        self.assertIsNone(result)
        mock_upload.assert_called_once_with(**data)

    @patch(resolve_patch('factory'))
    def test_download_from_filename(self, mock_factory):
        # Prepare and Mocks
        data = dict(
            filename='/a/-filename',
            bucket_name='a-bucket',
            key='a-key'
        )
        mock_upload = Mock(return_value=None)
        mock_factory.return_value.download_from_filename = mock_upload
        result = download_from_filename(**data)
        self.assertIsNone(result)
        mock_upload.assert_called_once_with(**data)

    @patch(resolve_patch('factory'))
    def test_get_file_url(self, mock_factory):
        # Prepare and Mocks
        data = dict(
            filename='/a/-filename',
            bucket_name='a-bucket',
            key='a-key'
        )
        mock_upload = Mock(return_value=None)
        mock_factory.return_value.get_file_url = mock_upload
        result = get_file_url(**data)
        self.assertIsNone(result)
        mock_upload.assert_called_once_with(**data)
