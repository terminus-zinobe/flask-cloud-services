from unittest import TestCase
from unittest.mock import Mock, patch
from flask_cloud_services.services.notifications import listener, publisher


def resolve_patch(path: str) -> str:
    return f'flask_cloud_services.services.notifications.{path}'


class TestNotifications(TestCase):

    @patch(resolve_patch('config.NOTIFICATION'), 'Notification')
    @patch(resolve_patch('factory'))
    def test_listener(self, mock_factory):
        @listener
        def func(data_listener):
            self.assertIsInstance(data_listener, Mock)
            self.assertEqual(data_listener.message_type, 'Notification')
            self.assertEqual(data_listener.response, 'ok')

        # Mocks
        mock_result = Mock(
            message_type='Notification',
            response="ok"
        )
        mock_factory.return_value.listener.return_value = mock_result
        result = func()
        self.assertEqual(result, "ok")
        mock_factory.return_value.listener.assert_called_once()

    @patch(resolve_patch('factory'))
    def test_publisher(self, mock_factory):
        # Mocks
        mock_factory.return_value.publisher = Mock(return_value='published')
        result = publisher()
        self.assertEqual(result, "published")
        mock_factory.return_value.publisher.assert_called_once()
