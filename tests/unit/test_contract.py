from unittest import TestCase
from flask_cloud_services.contract import (
    BusNotificationsContract,
    DataListener,
    StorageContract,
)


class TestClassBusNotifications(BusNotificationsContract):
    def listener(self):
        super().listener()
        pass

    def publisher(
        self,
        topic_arn: str,
        message
    ):
        super().publisher(topic_arn, message)
        pass


class TestClassStorage(StorageContract):

    def upload_from_filename(
        self,
        filename: str,
        bucket_name: str,
    ):
        super().upload_from_filename(
            filename=filename,
            bucket_name=bucket_name,
        )
        pass

    def upload_from_file_obj(
        self,
        file_obj,
        bucket_name: str,
    ):
        super().upload_from_file_obj(
            file_obj=file_obj,
            bucket_name=bucket_name,
        )
        pass

    def download_from_filename(
        self,
        filename: str,
        bucket_name: str,
        key: str,
    ):
        super().download_from_filename(
            filename=filename,
            bucket_name=bucket_name,
            key=key,
        )
        pass

    def get_file_url(
        self,
        bucket_name: str,
        key: str,
    ):
        super().get_file_url(
            bucket_name=bucket_name,
            key=key,
        )
        pass


class TestContract(TestCase):

    def test_bus_notifications_contract(self):
        contract = TestClassBusNotifications()
        rsp_listener = contract.listener()
        rsp_publisher = contract.publisher(
            topic_arn='topic',
            message='msg'
        )
        self.assertIsNone(rsp_listener)
        self.assertIsNone(rsp_publisher)

    def test_data_listener(self):
        params = dict(
            message_type='type',
            topic_arn='topic',
            message='msg',
            response='response'
        )
        data_listener = DataListener(**params)
        for key in params:
            self.assertEqual(getattr(data_listener, key), params[key])

    def test_storage_contract(self):
        contract = TestClassStorage()
        rsp_upload = contract.upload_from_filename(
            filename='file-name',
            bucket_name='bucket-name'
        )
        rsp_upload_obj = contract.upload_from_file_obj(
            file_obj='file-obj',
            bucket_name='bucket-name'
        )
        rsp_download = contract.download_from_filename(
            filename='file-name',
            bucket_name='bucket-name',
            key='any-key'
        )
        rsp_get_url = contract.get_file_url(
            bucket_name='bucket-name',
            key='any-key',
        )
        self.assertIsNone(rsp_upload)
        self.assertIsNone(rsp_upload_obj)
        self.assertIsNone(rsp_download)
        self.assertIsNone(rsp_get_url)
