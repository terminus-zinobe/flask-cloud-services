from unittest import TestCase
from unittest.mock import patch
from flask_cloud_services.provider import factory
from flask_cloud_services.provider.aws import BusNotifications


class TestFactory(TestCase):

    @patch('flask_cloud_services.config.PROVIDER', 'a provider')
    def test_factory_no_provider(self):
        self.assertRaises(Exception, factory, 'a service')

    @patch('flask_cloud_services.config.PROVIDER', 'aws')
    def test_factory_no_service(self):
        self.assertRaises(Exception, factory, 'a service')

    @patch('flask_cloud_services.config.PROVIDER', 'aws')
    def test_factory_ok(self):
        self.assertIsInstance(factory('BusNotifications'), BusNotifications)
