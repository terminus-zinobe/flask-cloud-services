import json
from unittest import TestCase
from unittest.mock import Mock, patch
from flask_cloud_services.provider.aws import (
    BusNotifications,
    Storage,
)


def resolve_patch(path: str) -> str:
    return f'flask_cloud_services.provider.aws.{path}'


class TestBusNotifications(TestCase):

    @patch(resolve_patch('DataListener'))
    def test_listener_bad_request(self, mock_data_listener):
        # Mocks
        mock_data_listener.return_value.response = ('bad_request', 400)

        result = BusNotifications().listener()
        self.assertEqual(result, mock_data_listener.return_value)
        self.assertEqual(result.response, ('bad_request', 400))

    @patch(resolve_patch('DataListener'))
    @patch(resolve_patch('requests.get'))
    @patch(resolve_patch('request'))
    def test_listener_notification(
        self,
        mock_request,
        mock_requests_get,
        mock_data_listener
    ):
        # Mocks
        mock_data_listener.return_value = Mock(
            response='OK\n',
            message_type='NOTIFICATION',
            message='msg',
            topic_arn='topic-prueba'
        )

        mock_request.headers = {
            'X-Amz-Sns-Message-Type': 'Notification',
            'X-Amz-Sns-Topic-Arn': 'topic-prueba'
        }
        data = {
            'Type': 'Notification',
            'MessageId': '4c3934cf-81dc-4c60-a5ef-9164111e4b7b',
            'Token': 'token',
            'TopicArn': 'topic-prueba',
            'Message': 'msg',
            'SubscribeURL': ('http://notification.test')
        }
        mock_request.data = json.dumps(data)
        result = BusNotifications().listener()
        self.assertEqual(result, mock_data_listener.return_value)
        self.assertEqual(result.response, 'OK\n')
        self.assertEqual(result.message, 'msg')
        self.assertEqual(result.message_type, 'NOTIFICATION')
        self.assertEqual(result.topic_arn, 'topic-prueba')

    @patch(resolve_patch('DataListener'))
    @patch(resolve_patch('requests.get'))
    @patch(resolve_patch('request'))
    def test_listener_suscription(
        self,
        mock_request,
        mock_requests_get,
        mock_data_listener
    ):
        # Mocks
        mock_data_listener.return_value = Mock(
            response='OK\n',
            message_type='NOTIFICATION',
            message='msg',
            topic_arn='topic-prueba'
        )

        mock_request.headers = {
            'X-Amz-Sns-Message-Type': 'SubscriptionConfirmation'
        }
        data = {
            'Type': 'SubscriptionConfirmation',
            'MessageId': '4c3934cf-81dc-4c60-a5ef-9164111e4b7b',
            'Token': 'token',
            'TopicArn': 'topic-prueba',
            'Message': 'msg',
            'SubscribeURL': ('http://confirm-suscription.test')
        }
        mock_request.data = json.dumps(data)
        result = BusNotifications().listener()
        self.assertEqual(result, mock_data_listener.return_value)
        self.assertEqual(result.response, 'OK\n')
        self.assertEqual(result.message, 'msg')
        self.assertEqual(result.message_type, 'NOTIFICATION')
        self.assertEqual(result.topic_arn, 'topic-prueba')
        mock_requests_get.assert_called_with('http://confirm-suscription.test')

    @patch(resolve_patch('DataListener'))
    @patch(resolve_patch('requests.get'))
    @patch(resolve_patch('request'))
    def test_listener_any(
        self,
        mock_request,
        mock_requests_get,
        mock_data_listener
    ):
        # Mocks
        mock_data_listener.return_value = Mock(
            response=('bad_request', 400),
        )

        mock_request.headers = {
            'X-Amz-Sns-Message-Type': 'Any_Type'
        }
        data = {
            'Type': 'Any_Type',
            'MessageId': '4c3934cf-81dc-4c60-a5ef-9164111e4b7b',
            'TopicArn': 'topic-prueba',
            'Message': 'msg',
        }
        mock_request.data = json.dumps(data)
        result = BusNotifications().listener()
        self.assertEqual(result, mock_data_listener.return_value)
        self.assertEqual(result.response, ('bad_request', 400))

    @patch(resolve_patch('boto3.client'))
    def test_publisher(self, mock_boto3):
        # Mocks
        mock_boto3.return_value.publish = Mock(return_value='value')
        response = BusNotifications().publisher(
            topic_arn='a topic',
            message='msg'
        )
        self.assertEqual(response, 'value')
        mock_boto3.return_value.publish.assert_called_with(
            TopicArn='a topic',
            Message='msg'
        )


class TestStorage(TestCase):

    def test_upload_from_filename(self):
        # Prepare and Mocks
        data = dict(
            filename='/a/filename',
            bucket_name='a-bucket',
            key='a-key'
        )
        error_msg = '[Errno 2] No such file or directory: \'/a/filename\''
        with self.assertRaises(FileNotFoundError) as error:
            # Exec
            Storage().upload_from_filename(**data)
        # Asserts
        self.assertEqual(str(error.exception), error_msg)

    def test_upload_from_file_obj(self):
        # Prepare and Mocks
        data = dict(
            file_obj=b'1',
            bucket_name='a-bucket',
            key='a-key'
        )
        error_msg = 'Fileobj must implement read'
        with self.assertRaises(ValueError) as error:
            # Exec
            Storage().upload_from_file_obj(**data)
        # Asserts
        self.assertEqual(str(error.exception), error_msg)

    def test_download_from_filename(self):
        # Prepare and Mocks
        data = dict(
            filename='/a/filename',
            bucket_name='a-bucket',
            key='a-key'
        )
        error_msg = 'Unable to locate credentials'
        with self.assertRaises(Exception) as error:
            # Exec
            Storage().download_from_filename(**data)
        # Asserts
        self.assertEqual(str(error.exception), error_msg)

    def test_get_file_url_exception(self):
        # Prepare and Mocks
        data = dict(
            bucket_name='a-bucket',
            key='a-key'
        )
        error_msg = 'Unable to locate credentials'
        with self.assertRaises(Exception) as error:
            # Exec
            Storage().get_file_url(**data)
        # Asserts
        self.assertEqual(str(error.exception), error_msg)

    def test_get_file_url_ok(self):
        # Prepare and Mocks
        data = dict(
            bucket_name='a-bucket',
            key='a-key'
        )
        url = 'https://s3-any.amazonaws.com/a-bucket/a-key'
        mock_client = Mock()
        mock_client.get_bucket_location.return_value = {
            'LocationConstraint': 'any'
        }
        storage = Storage()
        storage.CLIENT = mock_client
        # Exec
        result = storage.get_file_url(**data)
        # Asserts
        self.assertEqual(result, url)
